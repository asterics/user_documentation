**User documentation for ASTERICS: an online tool to perform statistical and integrative analyses**

This is the source repository for user documentation.

ASTERICS: https://asterics.miat.inrae.fr

Online documentation: https://asterics.pages.mia.inra.fr/user_documentation/
