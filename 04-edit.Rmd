# Edit a dataset {#editdataset}

Under this menu, you can perform various type of edition on your datasets:

- **Datasets can be transposed or corrected**. You can change column type, set
individual (row) names, subset individuals or variables and rename categories 
for your variables. 
- Once validated, these changes will result in a **new (corrected) dataset** in
your workspace. 


```{r, echo=FALSE, out.width="45%", fig.align='left'}
knitr::include_graphics('./images/edit/HelpREdit.png')
```

```{r, echo=FALSE, fig.align='center'}
if (knitr:::is_latex_output()) {
  par(mar = c(0,0,0,0))
  plot(c(0, 1), c(0, 1), ann = F, bty = 'n', type = 'n', xaxt = 'n', yaxt = 'n')
  text(x = 0.5, y = 0.5, 
       paste("The animated image is rendered in the online help only.\nhttps://asterics.pages.mia.inra.fr/user_documentation/editdataset.html#editdataset"), 
     cex = 1, col = "black")
} else {
  knitr::include_graphics('./images/edit/edit.gif')
}
```

